# Util Service Support

A library of support classes for integrating React components with REST HTTP services, including support for authentication.

## Installation

```sh
npm install @NAME@
```

## Using the component

Use the component in your React app as follows:

```
import { AbstractResource } from '@NAME@';

...
```
