import styled from 'styled-components';

// tslint:disable-next-line:variable-name
const DateGroup = styled.ol`
  list-style-type: none;
  padding-left: 0;
`;

export default DateGroup;
