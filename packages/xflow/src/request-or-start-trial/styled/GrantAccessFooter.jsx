import styled from 'styled-components';

const GrantAccessFooter = styled.div`
  text-align: right;
`;

GrantAccessFooter.displayName = 'GrantAccessFooter';
export default GrantAccessFooter;
